import std/core;
import std/range;

pragma generate_std_array_builtins;

template <T: type, Size: uint>
extend Array {
	func iterator(this: &Array!<T, Size>) -> RangeIterator!<T> {
		return RangeIterator!<T>(this.data(), this.data() + Size);
	}

	func iterator(this: &&Array!<T, Size>) -> VRangeIterator!<T> {
		return VRangeIterator!<T>(this.data(), this.data() + Size);
	}

	func at(this: &Array!<T, Size>, index: uint) -> Optional!<&T> {
		if index < 0u || index >= Size {
			return None;
		}

		return Some(&this.data()[index]);
	}

	operator [](this: &Array!<T, Size>, index: uint) -> &T {
		if index < 0u || index >= Size {
			panic(Error::EBOUNDS);
		}

		return &this.data()[index];
	}

	operator [](this: &&Array!<T, Size>, index: uint) -> &&T {
		if index < 0u || index >= Size {
			panic(Error::EBOUNDS);
		}

		return &&this.data()[index];
	}

	operator [](this: &Array!<T, Size>, range: NumericRange!<uint>) -> Range!<T> {
		if range.start < 0u || range.end > Size {
			panic(Error::EBOUNDS);
		}

		if range.start >= range.end || range.step != 1u {
			panic(Error::EINVAL);
		}

		return Range!<T>(this.data() + range.start, range.end - range.start);
	}

	operator [](this: &&Array!<T, Size>, range: NumericRange!<uint>) -> VRange!<T> {
		if range.start < 0u || range.end > Size {
			panic(Error::EBOUNDS);
		}

		if range.start >= range.end || range.step != 1u {
			panic(Error::EINVAL);
		}

		return VRange!<T>(this.data() + range.start, range.end - range.start);
	}

	func range(this: &Array!<T, Size>) -> Range!<T> {
		return Range!<T>(this.data(), this.size());
	}

	func range(this: &&Array!<T, Size>) -> VRange!<T> {
		return VRange!<T>(this.data(), this.size());
	}

	func find(this: &Array!<T, Size>, value: T) -> Optional!<uint> {
		for index in 0u..this.size() {
			if this.data()[index] == value {
				return Some(index);
			}
		}

		return None;
	}

	func contains(this: &Array!<T, Size>, value: T) -> bool {
		return this.find(value) is Some;
	}
}
